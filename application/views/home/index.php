<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta charset="utf-8">
    <title>Agenda Rektor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/icon" href="<?= base_url('assets/img/logo.png'); ?>">
    <link rel="stylesheet" href="<?= base_url() ?>assets/backend/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/styleku.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/backend/dist/css/adminlte.min.css">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- jQuery -->
    <script src="<?= base_url() ?>assets/backend/plugins/jquery/jquery.min.js"></script>
    <style>
        body {
            background-color: #484848;
            color: #ffffff;
            /* Ini buat warna teksnya biar kontras dengan background */
        }

        .container {
            max-width: 1800px;
            margin: 0 auto;
            /* Ini biar container berada di tengah layar */
        }
    </style>
</head>

<body>

    <div class="container pt-3">
        <table style="width:100%">
            <thead>
                <tr>
                    <th style="width:5%"><img src=" <?= base_url('assets/img/logo.png'); ?>" style="width:50px">
                    </th>
                    <th style="width:80%">
                        <h4><b>INFORMATION SCHEDULE SYSTEM<b></h3>
                    </th>
                    <th style="width:15%">
                        <h3><?= date('d F Y') ?></h3>
                        <h1 id="clock"></h1>
                    </th>
                </tr>
            </thead>

        </table>
        <div class="pt-2"></div>
        <table class="table table-dark table-striped" style="width:100%">
            <thead>
                <tr>

                    <th>Date</th>
                    <th>Time</th>
                    <th>Name</th>
                    <th>Title</th>
                    <th>Location</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>13 November 2023</td>
                    <td>08.00 WIB</td>
                    <td>Prof. Dr. Agussani, M.AP</td>
                    <th>Wisuda I</th>
                    <th>Selecta - Kota Medan</th>
                    <th><span class="badge badge-success">On Schedule</span></th>
                </tr>
                <tr>
                    <td>14 November 2023</td>
                    <td>08.00 WIB</td>
                    <td>Prof. Dr. Agussani, M.AP</td>
                    <th>Wisuda II</th>
                    <th>Selecta - Kota Medan</th>
                    <th><span class="badge badge-success">On Schedule</span></th>
                </tr>
                <tr>
                    <td>15 November 2023</td>
                    <td>08.00 WIB</td>
                    <td>Prof. Dr. Agussani, M.AP</td>
                    <th>Wisuda III</th>
                    <th>Selecta - Kota Medan</th>
                    <th><span class="badge badge-success">On Schedule</span></th>
                </tr>
                <tr>
                    <td>26 November 2023</td>
                    <td>08.00 WIB</td>
                    <td>Prof. Dr. Akrim, M.Pd</td>
                    <th>Wisuda I</th>
                    <th>Yogyakarta</th>
                    <th><span class="badge badge-success">On Schedule</span></th>
                </tr>
                <tr>
                    <td>13 November 2023</td>
                    <td>08.00 WIB</td>
                    <td>Prof. Dr. Agussani, M.AP</td>
                    <th>Wisuda II</th>
                    <th>Selecta - Kota Medan</th>
                    <th><span class="badge badge-success">On Schedule</span></th>
                </tr>
                <tr>
                    <td>14 November 2023</td>
                    <td>08.00 WIB</td>
                    <td>Prof. Dr. Agussani, M.AP</td>
                    <th>Wisuda III</th>
                    <th>Selecta - Kota Medan</th>
                    <th><span class="badge badge-success">On Schedule</span></th>
                </tr>
                <tr>
                    <td>15 November 2023</td>
                    <td>08.00 WIB</td>
                    <td>Prof. Dr. Agussani, M.AP</td>
                    <th>Wisuda I</th>
                    <th>Selecta - Kota Medan</th>
                    <th><span class="badge badge-success">On Schedule</span></th>
                </tr>
                <tr>
                    <td>26 November 2023</td>
                    <td>08.00 WIB</td>
                    <td>Prof. Dr. Akrim, M.Pd</td>
                    <th>Wisuda II</th>
                    <th>Yogyakarta</th>
                    <th><span class="badge badge-success">On Schedule</span></th>
                </tr>
                <tr>
                    <td>13 November 2023</td>
                    <td>08.00 WIB</td>
                    <td>Prof. Dr. Agussani, M.AP</td>
                    <th>Wisuda III</th>
                    <th>Selecta - Kota Medan</th>
                    <th><span class="badge badge-success">On Schedule</span></th>
                </tr>
                <tr>
                    <td>14 November 2023</td>
                    <td>08.00 WIB</td>
                    <td>Prof. Dr. Agussani, M.AP</td>
                    <th>Wisuda I</th>
                    <th>Selecta - Kota Medan</th>
                    <th><span class="badge badge-success">On Schedule</span></th>
                </tr>
                <tr>
                    <td>15 November 2023</td>
                    <td>08.00 WIB</td>
                    <td>Prof. Dr. Agussani, M.AP</td>
                    <th>Wisuda II</th>
                    <th>Selecta - Kota Medan</th>
                    <th><span class="badge badge-success">On Schedule</span></th>
                </tr>
                <tr>
                    <td>26 November 2023</td>
                    <td>08.00 WIB</td>
                    <td>Prof. Dr. Akrim, M.Pd</td>
                    <th>Wisuda III</th>
                    <th>Yogyakarta</th>
                    <th><span class="badge badge-success">On Schedule</span></th>
                </tr>
                <tr>
                    <td>13 November 2023</td>
                    <td>08.00 WIB</td>
                    <td>Prof. Dr. Agussani, M.AP</td>
                    <th>Wisuda I</th>
                    <th>Selecta - Kota Medan</th>
                    <th><span class="badge badge-success">On Schedule</span></th>
                </tr>
                <tr>
                    <td>14 November 2023</td>
                    <td>08.00 WIB</td>
                    <td>Prof. Dr. Agussani, M.AP</td>
                    <th>Wisuda II</th>
                    <th>Selecta - Kota Medan</th>
                    <th><span class="badge badge-success">On Schedule</span></th>
                </tr>
                <tr>
                    <td>15 November 2023</td>
                    <td>08.00 WIB</td>
                    <td>Prof. Dr. Agussani, M.AP</td>
                    <th>Wisuda III</th>
                    <th>Selecta - Kota Medan</th>
                    <th><span class="badge badge-success">On Schedule</span></th>
                </tr>
                <tr>
                    <td>26 November 2023</td>
                    <td>08.00 WIB</td>
                    <td>Prof. Dr. Akrim, M.Pd</td>
                    <th>Wisuda I</th>
                    <th>Yogyakarta</th>
                    <th><span class="badge badge-success">On Schedule</span></th>
                </tr>
            </tbody>
        </table>
    </div>

</body>
<script src="<?= base_url() ?>assets/backend/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>assets/backend/dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url() ?>assets/backend/dist/js/demo.js"></script>
<!-- SweetAlert2 -->
<script src="<?= base_url() ?>assets/backend/plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="<?= base_url() ?>assets/js/notif.js"></script>
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script type="text/javascript">
    var detik = <?php echo date('s'); ?>;
    var menit = <?php echo date('i'); ?>;
    var jam = <?php echo date('H'); ?>;

    function clock() {
        if (detik != 0 && detik % 60 == 0) {
            menit++;
            detik = 0;
        }
        second = detik;

        if (menit != 0 && menit % 60 == 0) {
            jam++;
            menit = 0;
        }
        minute = menit;

        if (jam != 0 && jam % 24 == 0) {
            jam = 0;
        }
        hour = jam;

        if (detik < 10) {
            second = '0' + detik;
        }
        if (menit < 10) {
            minute = '0' + menit;
        }

        if (jam < 10) {
            hour = '0' + jam;
        }
        waktu = hour + ':' + minute + ':' + second;

        document.getElementById("clock").innerHTML = waktu;
        detik++;
    }

    setInterval(clock, 1000);
</script>

</html>